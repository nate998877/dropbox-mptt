# Project Explanation
---
Kenzie Academy's Back End Assessment : Hierarchical Data and You

The stated goal of this assessment is to create an Modified Preorder Tree Traversal(MPTT) model and subsequent database entry of that model in django.
The creation of such database entries are to be done using the django-mptt package's draggable admin, with an extra credit opportunity to create a 
form for adding them without the admin panel. Any created database entries should be shown on the homepage, with an extra credit opportunity if a 
per-user system is created for displaying created trees.

# Requirements to Run

clone git repository
> git clone git@gitlab.com:nate998877/dropbox-mptt.git

install environment and activate environment
>pipenv install && pipenv shell

start django server
>python manage.py runserver


---
Project completed on 12/11/2019