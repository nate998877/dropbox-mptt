from dropbox.models import File
from django.shortcuts import render

def show_tree(request):
  return render(request, "homepage.html", {'tree': File.objects.all()})